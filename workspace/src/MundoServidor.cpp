// Mundo.cpp: implementation of the CMundo class.
//
//Alejandro Sacristan modifico esta cabecera 
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "Puntos.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <iostream>
#include <pthread.h>
#include <string>
#include <fcntl.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

//VARIABLES GLOBALES

#define NMAXDISPAROS 10
#define NMAXESFERAS 2
#define PERIODO 200
#define GOLPESMAX 2
int nDispJ1=0;
int nDispJ2=0;
int golp1=0;
int golp2=0;
bool bloq1=false;
bool bloq2=false;
//OPCIONAL
//funcion para cerrar con el SIGPIPE
static void CerrarServ (int n)
{
	exit(n);
}
static void ArmadoInt (int n){
	exit(n);
}
static void ArmadoTerm (int n){
	exit(n);
}
static void ArmadoUsr (int n){
	exit(0);
}
void *hilo_com(void* d){
	CMundoServidor* p=(CMundoServidor*) d;
	p->RecibeComJugador();
}
void CMundoServidor::RecibeComJugador(){
//abro fifo en lectura
//esto se puede comentar
fifo_cliente_servidor=open("/tmp/FIFO_CLIENTE_SERVIDOR",O_RDONLY);
if(fifo_cliente_servidor<0){
	perror("No puede abrirse el fifo");
	return;
}
while (1){
	usleep(10);
	char cad[100];
	read(fifo_cliente_servidor, cad, sizeof(cad));
	//s_comunicacion.Receive(cad,sizeof(cad));
	unsigned char key;
	sscanf(cad,"%c",&key);
	if(key=='s')jugador1.velocidad.y=-4;
	if(key=='w')jugador1.velocidad.y=4;
	if(key=='l')jugador2.velocidad.y=-4;
	if(key=='o')jugador2.velocidad.y=4;
	if(key=='d'){
		if(nDispJ1<NMAXDISPAROS){
		disparos.push_back(new Disparo(DERECHA,jugador1.getCentro()));
		nDispJ1+=1;
		}
	}
	if(key=='k'){
		if(nDispJ2<NMAXDISPAROS){
		disparos.push_back(new Disparo(IZQUIERDA, jugador2.getCentro()));
		nDispJ2+=1;
		}	
	}
	}
}
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();

}

CMundoServidor::~CMundoServidor()
{
	close(fd);
	unlink("/tmp/FIFO_LOGGER");
//esto se puede comentar
	close(fifo_servidor_cliente);
	close(fifo_cliente_servidor);
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	 for(i=0;i<esferas.size();i++)
                esferas[i]->Dibuja();
	 for(i=0;i<disparos.size();i++)
                disparos[i]->Dibuja();
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
static int timer=0;
Puntos p;
if(!bloq1)
jugador1.Mueve(0.025f);
if(!bloq2)
jugador2.Mueve(0.025f);
esfera.Mueve(0.025f);
	for(int i=0;i<esferas.size();i++){
		esferas[i]->Mueve(0.025f);
	}
	for(int i=0;i<disparos.size();i++){
		disparos[i]->Mueve(0.025f);
	}
	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	for(int j=0;j<esferas.size();j++){
		for(int i=0;i<paredes.size();i++){
			paredes[i].Rebota(*esferas[j]);
		}
	}
//se gestionan los disparos
for(int j=0;j<disparos.size();j++){
	bool destruirDisp=false;
	if(jugador1.recibeDisparo(*disparos[j])){
		jugador1.agrandar(-1);
		jugador1.velocidad.y=0;
		bloq1=true;
		destruirDisp=true;
		nDispJ2-=1;
	}
	   if(jugador2.recibeDisparo(*disparos[j])){
                jugador2.agrandar(-1);
                jugador2.velocidad.y=0;
                bloq2=true;
                destruirDisp=true;
                nDispJ1-=1;
        }
	if(fondo_izq.Rebota(*disparos[j])||fondo_dcho.Rebota(*disparos[j])){
		if(disparos[j]->d==DERECHA){nDispJ1-=1;}
		if(disparos[j]->d==IZQUIERDA){nDispJ2-=1;}
		destruirDisp=true;
	}
	if(destruirDisp){
		delete disparos[j];
		disparos.erase(disparos.begin()+j);
		disparos.shrink_to_fit();
	}
}
//se agranda raqueta cada cierto tiempo
 if(timer%PERIODO==0){
	jugador1.agrandar(1);
	jugador2.agrandar(1);
	timer=0;
	bloq1=false;
	bloq2=false;
}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	for(int i=0;i<esferas.size();i++){
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);
	}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		p.j1=puntos1;
		p.j2=puntos2;
		p.ganador=2;
		write(fd,&p,sizeof(p));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		p.j1=puntos1;
		p.j2=puntos2;
		p.ganador=1;
		write(fd, &p,sizeof(p));
	}
	for(int i=0;i<esferas.size();i++){
		if(fondo_izq.Rebota(*esferas[i])){
			esferas[i]->radio=0.5;
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			p.j1=puntos1;
			p.j2=puntos2;
			p.ganador=2;
			write(fd, &p, sizeof(p));
		}
		if(fondo_dcho.Rebota(*esferas[i])){
                        esferas[i]->radio=0.5;
                        esferas[i]->centro.x=0;
                        esferas[i]->centro.y=rand()/(float)RAND_MAX;
                        esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
                        esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
                        puntos1++;
                        p.j1=puntos1;
                        p.j2=puntos2;
                        p.ganador=1;
                        write(fd, &p, sizeof(p));
                }
}
		timer+=1;
//comunicacion servidor cliente
	char cad[200];
//AQUI HACER LO DE LA COMUNICACION DE LOS DISPAROS.
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);
	//esto se puede comentar al hacer la practica 5
	write(fifo_servidor_cliente,cad,sizeof(cad));
	//s_comunicacion.Send(cad,sizeof(cad));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'b':esferas.push_back(new Esfera); break;
	case 'd':
		if(nDispJ1<NMAXDISPAROS){
		disparos.push_back(new Disparo(DERECHA,jugador1.getCentro()));
		nDispJ1+=1;
		}
		break;
	case 'k':
		if(nDispJ2<NMAXDISPAROS){ 
                disparos.push_back(new Disparo(IZQUIERDA,jugador2.getCentro()));
                nDispJ2+=1;
                }
                break;
	}
}
void CMundoServidor::Init()
{
//SOCKET
/*char ip[]="192.168.1.47";
if(s_conexion.InitServer(ip,8000)==-1)
	printf("error al abrir servidor");
s_comunicacion=s_conexion.Accept();
//nombre cliente
char nombre[50];
s_comunicacion.Receive(nombre,sizeof(nombre));
printf("%s se ha conectado a la partida.\n",nombre);
*/
//inicio con logger fifo
if((fd=open("/tmp/FIFO_LOGGER",O_WRONLY))<0){
	perror("No puede abrirse el fifo");
	return;
}
//apertura fifo
//esto se puede comentar
fifo_servidor_cliente=open("/tmp/FIFO_SERVIDOR_CLIENTE", O_WRONLY);
if(fifo_servidor_cliente<0){
	perror("No puede abrirse el fifo");
	return;
}
//se arma la señal sigpipe
struct sigaction accion;
accion.sa_handler=&CerrarServ;
//accion.sa_flags=SA_SIGINFO;
if(sigaction(SIGPIPE, &accion, NULL)<0){
	perror("sigaction");
	return;
}
//se arma la señal sigint
struct sigaction act;
act.sa_handler=&ArmadoInt;
if(sigaction(SIGINT, &act, NULL)<0){
	perror("sigint");
	return;
}
//act.sa_flags=0;
//se arma la señal sigterm 
struct sigaction acterm;
acterm.sa_handler=&ArmadoTerm;
if(sigaction(SIGTERM, &acterm, NULL)<0){
	perror("sigterm");
	return;
}
//acterm.sa_flags=0;
//se arma la señal sigusr2
struct sigaction actusr;
actusr.sa_handler=&ArmadoUsr;
if(sigaction(SIGUSR2, &actusr, NULL)<0){
	perror("sigusr");
	return;
}
//acturs.sa_flags=0;
//thread
pthread_attr_init(&atrib);
pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
pthread_create(&thid1, &atrib, hilo_com, this);




	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
