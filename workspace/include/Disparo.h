// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////
#include "Plano.h"
#include "Vector2D.h"
typedef enum{DERECHA, IZQUIERDA}direccion;
class Disparo: public Plano  
{
public:	
	Vector2D velocidad;
	direccion d;

	Disparo(direccion d, Vector2D pos);
	virtual ~Disparo();

	void Mueve(float t);
	void Dibuja();
};

