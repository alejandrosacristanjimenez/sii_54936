// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////
//Documento de Alejandro Sacristan Jimenez

#include "Esfera.h"
#include "glut.h"
#include <stdlib.h>
#include<time.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	srand(time(NULL));
	radio=0.5f;
	velocidad.x=rand()%10-5;
	velocidad.y=rand()%10-5;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro =centro+velocidad*t;//Para hacer que la pelota se mueva
//cambioTam(0.001);//Para hacer que la pelota disminuya su tamaño poco a poco.
}
