#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main(){
	int fd;
	DatosMemCompartida *puntdatosCompartidos;
	fd=open("/tmp/datosCompartidos",O_RDWR);
	if(fd<0){
	perror("error al abrir");
	exit(1);
	}

	puntdatosCompartidos=static_cast<DatosMemCompartida*>(mmap(0,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
	if(puntdatosCompartidos==MAP_FAILED){
	perror("error en la proyeccion");
	close(fd);
	return(1);
	}
	close (fd);
while(puntdatosCompartidos!=NULL){
if(puntdatosCompartidos->esfera.centro.y<puntdatosCompartidos->raqueta1.getCentro().y){
	puntdatosCompartidos->accion=-1;
}else if(puntdatosCompartidos->esfera.centro.y==puntdatosCompartidos->raqueta1.getCentro().y){
	puntdatosCompartidos->accion=0;
}else if (puntdatosCompartidos->esfera.centro.y>puntdatosCompartidos->raqueta1.getCentro().y){
	puntdatosCompartidos->accion=1;
}
usleep(25000);
}

unlink("/tmp/datosCompartidos");
return 1;
}
